const compareImages = require("resemblejs/compareImages");
const mzfs = require("mz/fs");
const fs = require('fs');
const renamePaths = false;

var imagePath = "C:/projects/test-runner/tmp/screenshots/";

var imageWithError = 0;

if (process.argv[2] !== undefined) {
	var imagePath = process.argv[2];
}

async function getImageDiff(predeployImagePath, postdeployImagePath, outputImagePath) {
    const options = {
        output: {
            errorColor: {
                red: 255,
                green: 0,
                blue: 255,
                brightness: 255
            },
            errorType: "movement",
            transparency: 0.3,
            largeImageThreshold: 1200,
            useCrossOrigin: false,
            outputDiff: true,
            /*ignoredBox: {
                left: 100,
                top: 200,
                right: 200,
                bottom: 600
            },*/
            boundingBox: {
                left: 265,
                top: 155,
                right: 1580,
                bottom: 920
            }
        },
        scaleToSameSize: true,
        ignore: "antialiasing"
    };

    const data = await compareImages(
        await mzfs.readFile(predeployImagePath),
        await mzfs.readFile(postdeployImagePath),
        options
    );

    var precent = 100 - data.rawMisMatchPercentage.toPrecision(1);

    if ((100 - data.rawMisMatchPercentage.toPrecision(5)) < 99.8) {
		var filePath = outputImagePath.replace(imagePath + 'diff/', '');
		var fileUrl = '/tmp/screenshots/diff/' + filePath;
		
		if (imageWithError == 0) {
			console.log('Error found:');
			console.log('');
		}

		console.log('<a target="_blank" href="' + fileUrl + '">' + filePath + '</a> - ' + precent + '% ');
		imageWithError++;
    }

    await mzfs.writeFile(outputImagePath, data.getBuffer());
}

function rimraf(dirPath) {
    if (fs.existsSync(dirPath)) {
		if (fs.readdirSync(dirPath).length > 0) {
			fs.readdirSync(dirPath).forEach(function(entry) {
				var entryPath = dirPath + entry;

				if (fs.lstatSync(entryPath).isDirectory()) {
					rimraf(entryPath + '/');
				} else {
					fs.unlinkSync(entryPath);
				}
			});

		}

		fs.rmdirSync(dirPath);
    }
}

if (fs.existsSync(imagePath + 'diff/')) {
    rimraf(imagePath + 'diff/');
}

if (fs.existsSync(imagePath + 'old_predeploy/')) {
    rimraf(imagePath + 'old_predeploy/');
}

if (fs.existsSync(imagePath + 'old_postdeploy/')) {
    rimraf(imagePath + 'old_postdeploy/');
}

if (!(fs.existsSync(imagePath + 'predeploy/') && fs.existsSync(imagePath + 'postdeploy/'))) {
    console.log('Process directories not found in ' + imagePath);
    process.exit(1);
}

fs.mkdirSync(imagePath + 'diff/', 0777);

function isDir(path) {
    try {
        var stat = fs.lstatSync(path);

        return stat.isDirectory();
    } catch (e) {
        return false;
    }
}

function onError (err) {
	console.log();
	console.log(err);

	return;
}

function readDirRecursively(imagePath, dirPath, filePath) {
	fs.readdir(imagePath + dirPath + filePath, function(err, filenames) {
		if (err) {
			onError(err);
			return;
		}

		filenames.forEach(function(filename) {
			var preDeployFilePath = imagePath + dirPath + filePath + filename;
			var postDeployFilePath = imagePath + "postdeploy/" + filePath + filename;
			var outputImagePath = imagePath + 'diff/' + filePath + filename;

			if (isDir(preDeployFilePath)) {
				if (!fs.existsSync(outputImagePath)) {
					fs.mkdirSync(outputImagePath, 0777);
				}

				readDirRecursively(imagePath, dirPath, filePath + filename + '/');
			} else {
				if (fs.existsSync(postDeployFilePath)) {
					getImageDiff(preDeployFilePath, postDeployFilePath, outputImagePath);
				}
			}
		});
	});
}

readDirRecursively(imagePath, "predeploy/", "");

/*if (renamePaths) {
	setTimeout(function() {
		fs.rename(imagePath + "predeploy/", imagePath + "old_predeploy/", function (err) {
			if (err) {
				onError(err);
				return;
			}
		});

		fs.rename(imagePath + "postdeploy/", imagePath + "old_postdeploy/", function (err) {
			if (err) {
				onError(err);
				return;
			}
		});
	}, 20000);
}*/