<?php
$action = $_GET['action'] ?? null;

switch ($action) {
    case "backup":
        break;
    case "rollback":
        break;
    default:
		throw new \Exception('Action not supported');
}

$basePath = dirname(__FILE__).'/protected';
shell_exec("php {$basePath}/yiic testRunner '{$action}' --environment=selenium --domain=selenium.ilgbusiness.com > /dev/null 2>&1");
echo '<html><head></head><body>OK<body></html>';