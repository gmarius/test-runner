<!DOCTYPE html>
<html lang="en-US">
    <head>
        <style type="text/css">
			html {
				font-family: "Open Sans", sans-serif;
				font-size: 15px;
				color: #222630;
			}
		
            * {
                box-sizing: border-box;
            }

            .hidden {
                display: none;
            }
            
            .form-group {
                margin: 5px;
                padding: 5px;
            }
            
            .form-group label {
                margin-bottom: 2px;
                display: block;
				font-weight: 600;
				font-size: 13px;
				
            }
            
            .form-help {
                margin: 10px;
                padding: 10px;
                background: #fdfde6;
            }
			
			input[type="text"], textarea {
				padding: 3px 5px;
				width: 500px;
			}
			
			input[type="submit"] {
				width: auto;
			}
            
            .output-frame {
                border: 1px solid #e4e4e4;
                margin: 2px;
                width: 1572px;
                padding: 5px;
            }
            
            .output-frame > label {
                display: block;
				padding-top: 6px;
				padding-left: 10px;
            }
            
            .output-frame .output-screen {
                float: left;
                width: 500px;
                margin: 10px;
            }
            
            .output-frame .output-screen .contents {
                border: 1px solid #e4e4e4;
                height: 400px;
                word-break: break-all;
                overflow-y: scroll;
                padding: 10px;
				line-height: 24px;
            }
			
			.output-frame .output-screen label {
				font-weight: 600;
				font-size: 13px;
				paddig-bottom: 4px;
			}
			
			a {
				color: #000;
			}
			
			#output-results {
				width: 1020px;
			}
			
			#output-commands {
				width: 1540px;
			}
        </style>
    </head>
    <body>
        <form action="?action=runTest" method="post" id="test-runner-form">
            <div class="form-group">
                <label>Old branch</label>
                <input type="text" name="runTest[oldBranch]" value="<?= $defaultValues['oldBranch'] ?? '' ?>">
            </div>
            <div class="form-group">
                <label>New branch</label>
                <input type="text" name="runTest[newBranch]" value="<?= $defaultValues['newBranch'] ?? '' ?>">
            </div>
            <div class="form-group">
                <label>Git repository (ssh link, not https)</label>
                <input type="text" name="runTest[gitRepo]" value="<?= $defaultValues['gitRepo'] ?? '' ?>" onchange="this.value = this.value.split(' ').slice(-1).pop()">
            </div>
            <div class="form-group">
                <label>Git ssh private key <a href='#' onclick="document.getElementById('git-ssh-help').classList.toggle('hidden')">(get key)</a></label>
                <textarea name="runTest[gitKey]" rows="3"><?= $defaultValues['gitKey'] ?? '' ?></textarea>
            </div>
            <div id="git-ssh-help" class="hidden form-help">
                <label>Generated public key (You can add this to your git repo)</label>
                <pre><?= $defaultKey['public'] ?></pre>
                <label>Generated private key (You can use this key for deployment then)</label>
                <pre><?= $defaultKey['private'] ?></pre>
            </div>
            <div class="form-group">
                <label>Test environment endpoint url (with http/https)</label>
                <input type="text" name="runTest[endpoint]" value="<?= $defaultValues['endpoint'] ?? '' ?>">
            </div>
            <div class="form-group">
                <label>AWS application name (In Elastic Beanstalk - All Applications list)</label>
                <input type="text" name="runTest[awsApplicationName]" value="<?= $defaultValues['awsApplicationName'] ?? '' ?>">
            </div>
            <div class="form-group">
                <label>AWS environment name (In Elastic Beanstalk - All Applications list)</label>
                <input type="text" name="runTest[awsEvironmentName]" value="<?= $defaultValues['awsEvironmentName'] ?? '' ?>">
            </div>
            <div class="form-group">
                <label>EC2 Keypair name (In Elastic Beanstalk - environment - configuration - security)</label>
                <input type="text" name="runTest[awsKeypairName]" value="<?= $defaultValues['awsKeypairName'] ?? '' ?>">
            </div>
            <div class="form-group">
                <label>Platform name (eb)</label>
                <input type="text" name="runTest[awsPlatformName]" value="<?= $defaultValues['awsPlatformName'] ?? 'PHP' ?>">
            </div>
            <div class="form-group">
                <label>AWS access key id</label>
                <input type="text" name="runTest[awsAccessKey]" value="<?= $defaultValues['awsAccessKey'] ?? '' ?>">
            </div>
            <div class="form-group">
                <label>AWS secret key</label>
                <input type="text" name="runTest[awsSecret]" value="<?= $defaultValues['awsSecret'] ?? '' ?>">
            </div>
            <div class="form-group">
                <label>AWS region</label>
                <input type="text" name="runTest[awsRegion]" value="<?= $defaultValues['awsRegion'] ?? 'eu-west-1' ?>">
            </div>
            <div class="form-group">
                <input type="hidden" name="action" value="runTest">
                <input type="submit" value="Run test with deployment"/>
				<input type="button" value="Run test" id="simple-test" />
            </div>
        </form>
        
        <div class="output-frame">
            <label>
                Auto update output: <input type="checkbox" id="test-runner-update-toggle" /><br>
                <span>Content updater handle id: <span id="test-runner-content-updater-handle-id">-</span></span>
            </label>
            <div class="output-screen" id="output-output"><label>Testing progress</label><div class="contents"></div></div>
            <div class="output-screen" id="output-results"><label>Testing results</label><div class="contents"></div></div>
            <div class="output-screen" id="output-image-old"><label>Image compare (old branch)</label><div class="contents"></div></div>
            <div class="output-screen" id="output-image-diff"><label>Image compare (difference)</label><div class="contents"></div></div>
            <div class="output-screen" id="output-image-new"><label>Image compare (new branch)</label><div class="contents"></div></div>
            <div class="output-screen" id="output-commands"><label>Shell commands</label><div class="contents"></div></div>
            <div style="clear:both"><div/>
        </div>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script>
        var xi = 1;
			$(document).on('click', '#simple-test', function() {
				var $this = $(this);
				var $form = $this.closest('form');
				
				$form.attr('action', '?action=runSimpleTest');
				$form.submit();
			});
		
            $(document).on('submit', '#test-runner-form', function() {
                var $this = $(this);

                $.post( $this.attr('action'), $this.serialize(), function() {
                    $('#test-runner-update-toggle').prop('checked', true);
                    clearInterval($this.get(0).contentUpdaterHandle);
                    $this.get(0).contentUpdaterHandle = setInterval(function() {
                        $('#test-runner-content-updater-handle-id').html($this.get(0).contentUpdaterHandle);

                        if ($('#test-runner-update-toggle').is(':checked')) {
                            $.post(
                                '?action=getTestOutput',
                                $this.serialize(),
                                function(response) {
                                   if (response.data) {
                                       $('#output-output').find('.contents').html(response.data.output);
                                       $('#output-commands').find('.contents').html(response.data.commands);
									   $('#output-results').find('.contents').html(response.data.results);
									   
									   $('#output-image-old').find('.contents').html(response.data.predeploy);
									   $('#output-image-diff').find('.contents').html(response.data.diff);
									   $('#output-image-new').find('.contents').html(response.data.postdeploy);
                                   }
                               },
                               'json'
                            );
                        }
                    }, 1000);
                });

                return false;
            });
        </script>
    </body>
</html>
