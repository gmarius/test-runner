# SeleniumTest

Install
========
- node -v If lower download node higher then 7.6 using msi install on windows
	
	cd C:\xampp\htdocs\seleniumtest\
	
	npm install resemblejs
	
	npm rebuild
	
	npm install mz
	
	npm install bluebird

	npm install any-promise
	
	npm install cypress

	node C:\xampp\htdocs\seleniumtest\compareTestResults.js

- If not works try to install
	
	npm install -g resemblejs
	
	npm install -g bluebird
	
	npm install -g any-promise
	
	npm install -g mz
	
	npm install -g cypress
	
	npm rebuild

How to run tests
========

	cd /code/
	node_modules/.bin/cypress run --spec "cypress/integration/login/*" --env configFile=production --config screenshotsFolder=tmp/screenshots/postdeploy
	node /code/compareTestResults.js /code/tmp/screenshots/


	node_modules/.bin/cypress run --spec "cypress/integration/login/*" --env configFile=production --config screenshotsFolder=tmp/screenshots/predeploy
	node_modules/.bin/cypress run --spec "cypress/integration/login/*" --env configFile=production --config screenshotsFolder=tmp/screenshots/postdeploy
	node /var/app/current/compareTestResults.js /code/tmp/screenshots/

How to test
========
	npm install cypress@3.4.1 --save-dev
	node_modules/.bin/cypress open
	node compareTestResults.js 'C:\projects\test-runner\public\tmp\screenshots\'
	