/// <reference types="Cypress" />

context('Login', () => {
	it('Post on Team feed', function() {
		
		cy.visit('/index.php?r=site/login')
		
		cy.get('#LoginForm_username').type(Cypress.env('email'))
		cy.get('#LoginForm_password').type(Cypress.env('password'))
		
		cy.get('#login-form').submit()
		
		cy.screenshot('login-complete')
		
		cy.get('[href="/index.php?r=newsFeed/index"][data-toggle="menu"]').click()
		
		cy.scrollTo(0).screenshot('team-feed-page')
		
		cy.get('.page-content-col-left .new-title-wrapper > .content-editable-editor').type('SELENIUM TEST TITLE 1')
		cy.get('.page-content-col-left .new-body-wrapper > .content-editable-editor').type('SELENIUM TEST CONTENT 1')
		cy.get('.news-feed-textarea-wrapper > .news-feed-button-wrapper > .btn-green').click()
		cy.wait(500)
		
		cy.scrollTo(0).screenshot('create-news-feed-item')
		
		cy.get('#news-feed-pages .news-feed-container .news-feed-entry:first .news-feed-select > .btn.dropdown-toggle').click()
		cy.get('#news-feed-pages .news-feed-container .news-feed-entry:first .news-feed-select > ul.news-feed-select-options > li:last').click()
		
		cy.scrollTo(0).screenshot('delete-news-feed-item')
	})

})
