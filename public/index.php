<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $action = $_GET['action'] ?? 'index';
    $params = $_POST;
    $defaultValues = (array)json_decode(isset($_COOKIE['runTest']) ? base64_decode($_COOKIE['runTest']) : '[]', true);

	class TestRunner {
		public $endpoint; // @TODO: split to variables
		
		public function runTests()
		{
            set_time_limit(60 * 60);

			/**
				calls [diff] script with [wipe] option
				...
				calls [checkout] passing [oldBranch]
				calls [deploy] passing [endpoint]
				calls [backup] passing [endpoint]
				calls [test] passing [endpoint]
				calls [rollback] passing [endpoint]

				calls [checkout] passing [newBranch]
				calls [deploy] passing [endpoint]
				calls [backup] passing [endpoint]
				calls [test] passing [endpoint]
				calls [rollback] passing [endpoint]
				...
				calls [diff] script with run [option]
				results
			*/

            try {
				$this->clearScreenshots();
                $this->cleanlogs();
                fastcgi_finish_request();
				
				
                $this->runAndLog('afterInstall');
                $this->runAndLog('gitClone');
                $this->runAndLog('gitCheckout', [true]);
                $this->runAndLog('beforeDeploy');
                $this->runAndLog('deploy');

				$this->runAndLog('backup');
				$this->runAndLog('testEnvironment', [true]);
				$this->loadImages(true);
                $this->runAndLog('rollBack');


				$this->runAndLog('gitCheckout', [false]);
                $this->runAndLog('beforeDeploy');
                $this->runAndLog('deploy');

				$this->runAndLog('backup');
				$this->runAndLog('testEnvironment', [false]);
				$this->runAndLog('rollBack');
				
				$this->compareResults();
				$this->loadImages();

                $this->log('All finished successfully');
            } catch (Exception $e) {
                $this->log("Process ended with errors:");
                $this->log($e->getMessage());
            }
		}
		
		public function runSimpleTest()
		{
            set_time_limit(60 * 60);

            try {
				
				$this->clearScreenshots();
                $this->cleanlogs();
                fastcgi_finish_request();

				$this->runAndLog('backup');
				$this->runAndLog('testEnvironment', [true]);
				$this->runAndLog('rollBack');
				
				$this->loadImages(true);

                $this->log('All finished successfully');
            } catch (Exception $e) {
                $this->log("Process ended with errors:");
                $this->log($e->getMessage());
            }
		}

		protected function clearScreenshots()
		{
			$systemPath = $this->getSystemPath();

			$this->shellExec("rm -rf {$systemPath}/tmp/screenshots/old_predeploy/");
			$this->shellExec("rm -rf {$systemPath}/tmp/screenshots/old_postdeploy/");

			rename("{$systemPath}/tmp/screenshots/predeploy/", "{$systemPath}/tmp/screenshots/old_predeploy/");
			rename("{$systemPath}/tmp/screenshots/postdeploy/", "{$systemPath}/tmp/screenshots/old_postdeploy/");
			
		}

        protected function cleanlogs($logFiles = ['output', 'commands', 'results', 'images_predeploy', 'images_postdeploy', 'images_diff'])
		{
            $systemPath = $this->getSystemPath();

            foreach ($logFiles as $logFile) {
                $filePath = "{$systemPath}/runtime/{$logFile}";
                
                if (file_exists("{$systemPath}/runtime/{$logFile}")) {
                    unlink($filePath);
                }
            }
        }
        
        protected function runAndLog($methodName, $arguments = [])
        {
            $this->log("{$methodName} has started");
            $this->$methodName(...$arguments);
            $this->log("{$methodName} has finished");
        }
        
        protected function log($text, $file = 'output')
        {
            $systemPath = $this->getSystemPath();
            $logTime = date('Y-m-d H:i:s');
            file_put_contents("{$systemPath}/runtime/{$file}", "{$logTime} \"{$text}\"\r\n", FILE_APPEND);
        }
        
        public function afterInstall()
        {
            $systemPath = $this->getSystemPath();

            $this->shellExec("cd {$systemPath} && npm install && node_modules/.bin/cypress install cypress@3.4.1");
        }
        
        public function gitClone()
        {
            $systemPath = $this->getSystemPath();
            $repoEscaped = escapeshellarg($this->options['gitRepo']);
			$keyEscaped = escapeshellarg($this->options['gitKey']);

            $this->shellExec("touch /var/www/.ssh/known_hosts");
            $this->shellExec("ssh-keygen -R bitbucket.org");
            $this->shellExec("ssh-keyscan -t rsa bitbucket.org >> /var/www/.ssh/known_hosts");
			$this->shellExec("rm -f /var/www/.ssh/id_rsa && echo {$keyEscaped} >> /var/www/.ssh/id_rsa && chmod 0600 /var/www/.ssh/id_rsa && rm -f /var/www/.ssh/id_rsa.pub");
			$this->shellExec([
				"cd {$systemPath}/tmp/",
				"rm -rf project", // If process running directory can't be deleted, I use sleep PC  to resolve this issue
				"mkdir -p project",
				"'yes'| git clone -v '{$repoEscaped}' project",
			]);
        }

		public function gitCheckout($isOldBranch)
		{
            $systemPath = $this->getSystemPath();

            $branchNameEscaped = escapeshellarg($isOldBranch ? $this->options['oldBranch'] : $this->options['newBranch']);
            $this->shellExec(
				[
					"cd {$systemPath}/tmp/project",
					"(git checkout --force test-runner-tmp || git checkout -b test-runner-tmp)",
					"git fetch --all",
				]
			);
            $this->shellExec("cd {$systemPath}/tmp/project && git branch -D {$branchNameEscaped}", false, false);
            $this->shellExec(
				[
                    "cd {$systemPath}/tmp/project",
					"git checkout --force origin/{$branchNameEscaped}",
					"cp -a {$systemPath}/deploymentResources/. {$systemPath}/tmp/project/",
					"curl -s https://getcomposer.org/installer | php",
					"git add .",
				]
			);

            $this->shellExec([// @TODO: make before and after methods and move this command there
                "cd {$systemPath}/tmp/project",
				"php composer.phar update",
                "npm install lessc",
                "npm install uglifyjs",
                "npm install eslint",
                "npm install browserify",
                "npm install postcss",
                "npm install cssnano",
				"npm install cross-env",
                "npm install --y",
            ]);
		}
        
        public function beforeDeploy()
        {
            $systemPath = $this->getSystemPath();

			$this->shellExec([
				"cd {$systemPath}/tmp/project",
				"npm run build"
			]);
        }

		public function deploy()
		{
            $systemPath = $this->getSystemPath();
            $regionEscaped = escapeshellarg($this->options['awsRegion']);
            $accessKeyEscaped = escapeshellarg($this->options['awsAccessKey']);
            $secretEscaped = escapeshellarg($this->options['awsSecret']);
            $platformNameEscaped = escapeshellarg($this->options['awsPlatformName']);
            $keyNameEscaped = escapeshellarg($this->options['awsKeypairName']);
            $appNameEscaped = escapeshellarg($this->options['awsApplicationName']);
            $environmentNameEscaped = escapeshellarg($this->options['awsEvironmentName']);

            $this->shellExec([
                "cd {$systemPath}/tmp/project",
                "export AWS_ACCESS_KEY_ID={$accessKeyEscaped}",
                "export AWS_SECRET_ACCESS_KEY={$secretEscaped}",
                "export AWS_DEFAULT_REGION={$regionEscaped}",
                "eb init --no-verify-ssl -r {$regionEscaped} -p {$platformNameEscaped} -k {$keyNameEscaped} {$appNameEscaped}",
                "eb use {$environmentNameEscaped} && eb deploy"
            ]);
		}

		public function backup()
        {
            $this->callUrl("{$this->options['endpoint']}/testRunner.php?action=backup");
        }

        public function rollBack()
        {
            $this->callUrl("{$this->options['endpoint']}/testRunner.php?action=rollback");
        }

        protected function callUrl($url) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60 * 60);

            $output = curl_exec($ch);
            curl_close($ch);

            return $output;
        }
        
        public function testEnvironment($isOldBranch)
        {
			$systemPath = $this->getSystemPath();
            $screenshotsDir = $isOldBranch ? 'predeploy' : 'postdeploy';
            $response = $this->shellExec(
				"cd {$systemPath} && node_modules/.bin/cypress run " 
				. "--spec \"cypress/integration/login/*\" "
				. "--env configFile=production "
				. "--config screenshotsFolder=tmp/screenshots/{$screenshotsDir}",
				true
			);
			$this->log($response, 'results');
        }
        
        public function compareResults()
        {
            $systemPath = $this->getSystemPath();
            $response = $this->shellExec("cd {$systemPath} && node {$systemPath}/compareTestResults.js {$systemPath}/tmp/screenshots/", true);
			$this->log($response, 'results');
        }

        public function loadImages($isOldBranch = false)
        {
            $systemPath = $this->getSystemPath();

			if ($isOldBranch) {
				$files = ['predeploy'];
			} else {
				$files = ['predeploy', 'postdeploy', 'diff'];
			}

			foreach ($files as $file) {
				$output = '';
				$images = $this->getFiles("{$systemPath}/tmp/screenshots/{$file}");
				
				foreach ($images as $image) {
					$imageView = str_replace("{$systemPath}/tmp/screenshots/{$file}", '', $image);
					$imageUrl = str_replace($systemPath, '', $image);
					
					$output .= "<a target=\"_blank\" href=\"{$imageUrl}\">{$imageView}</a>\r\n";
				}

				file_put_contents("{$systemPath}/runtime/images_{$file}", $output);
			}
        }

		private function getFiles($dir, &$results = array()){
			$files = scandir($dir);

			foreach($files as $key => $value){
				$path = realpath($dir . DIRECTORY_SEPARATOR . $value);
				if (!is_dir($path)) {
					$results[] = $path;
				} else if($value != "." && $value != "..") {
					$this->getFiles($path, $results);
				}
			}

			return $results;
		}

		public function __construct($options)
		{
			$this->options = $options;
		}
        
        protected function getSystemPath()
        {
            return dirname(__FILE__);
        }

		/**
		 * shellExec shell command
		 * @param array|string $command
		 */
		protected function shellExec($command, $returnOutput = false, $breakOnError = true) {
			if (is_array($command)) {
				$command = implode(' && ', $command);
			}

            exec("($command) 2>&1", $output, $return);
            
            if (is_array($output)) {
                $output = implode("\r\n", $output);
            }

            if ($return != 0) {
                $this->log("Error using command: {$command}\r\n{$output}\r\n\r\n", 'commands');

                if ($breakOnError) {
                    throw new \Exception("An error has occurred!\r\nUsing command: {$command}\r\n{$output}\r\n");
                }

                return false;
            } else {
                $this->log("Success using command: {$command}\r\n{$output}\n\n", 'commands');
            }

            return $returnOutput ? $output : true;
        }
        
        public function renderPage($filePath, $variables) // @TODO: create basic controller class and move this there
        {
            extract($variables);

            if (!file_exists($filePath)) {
                throw new Exception('Page does not exist');
            }

            ob_start();
            include $filePath;

            return ob_get_clean();
        }
	}

    $testRunner = new TestRunner($defaultValues);
    
    switch ($action) {
        case 'runTest':
            setcookie('runTest', base64_encode(json_encode($params['runTest'])), 2147483647);
            $defaultValues = array_merge($defaultValues, $params['runTest']);
            $testRunner->runTests();
        break;

		case 'runSimpleTest':
            setcookie('runTest', base64_encode(json_encode($params['runTest'])), 2147483647);
            $defaultValues = array_merge($defaultValues, $params['runTest']);
            $testRunner->runSimpleTest();
        break;

        case 'getTestOutput':
            header('Content-Type: application/json');

            echo json_encode([
                'data' => [
                    'output' => file_exists('runtime/output') ? str_replace("\n", "<br>", file_get_contents('runtime/output')) : '', // @TODO: hardcode
                    'commands' => file_exists('runtime/commands') ? str_replace("\n", "<br>", file_get_contents('runtime/commands')) : '',
                    'results' => file_exists('runtime/results') ? str_replace("\n", "<br>", file_get_contents('runtime/results')) : '',
					'predeploy' => file_exists('runtime/images_predeploy') ? str_replace("\n", "<br>", file_get_contents('runtime/images_predeploy')) : '',
					'diff' => file_exists('runtime/images_diff') ? str_replace("\n", "<br>", file_get_contents('runtime/images_diff')) : '',
					'postdeploy' => file_exists('runtime/images_postdeploy') ? str_replace("\n", "<br>", file_get_contents('runtime/images_postdeploy')) : '',
                ],
            ]);
        break;

        case 'index':
        default:
            echo $testRunner->renderPage(
                'views/index.php',
                [
                    'defaultValues' => $defaultValues, 'defaultKey' => [
                        'public' => shell_exec('cat "/tmp/default_id_rsa_pub_exposed"'),
                        'private' => shell_exec('cat "/tmp/default_id_rsa_exposed"'),
                    ]
                ]
            );
    }
